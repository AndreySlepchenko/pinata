const applicationSize = { w: 1015.625, h: 650 };

class GameApp extends PIXI.Application {
    resize(size) {
        let w = size.width,
            h = size.height;
        this.view.width = w;
        this.view.height = h;
        this.renderer.resize(w, h);

        let scale = Math.min(w / applicationSize.w, h / applicationSize.h);

        this.stage.scale.set(scale);

        this.stage.position.set(
            (w - applicationSize.w * scale) * 0.5,
            (h - applicationSize.h * scale) * 0.5
        );
    }
}

const app = new GameApp(applicationSize.w, applicationSize.h, {
    backgroundColor: 0x0fff22f,
});

// right click === left click
app.view.addEventListener('contextmenu', evt => { evt.preventDefault(); });

let currentSize = { width: 0, height: 0 };
let detectResizeWindow = setInterval(function () {
    if (
        app.view.width === window.innerHeight
        &&
        app.view.height === window.innerWidth
    ) return;

    currentSize.width = window.innerWidth;
    currentSize.height = window.innerHeight;

    app.resize(currentSize);
}, 500);

document.body.appendChild(app.view);

let Application = PIXI.Application,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    Sprite = PIXI.Sprite,
    canvasW = app.view.width,
    canvasH = app.view.height,
    pinataContainer = new PIXI.Container(),
    basicUi = new PIXI.Container(),
    secretsContainer = new PIXI.Container(),
    gameEndScene = new PIXI.Container(),
    particleStorage = new PIXI.particles.ParticleContainer(),
    pinataDeathConfig,
    pinataHitConfig,
    pinataHitEmitter,
    pinataDeathEmitter;

loader.add([
    'img/generalGameItems.json',
    'resources/emitter.json',
    'resources/emitter(candyExplosion).json',
    'img/menu.json',
]).load(onResoucesLoad);

function onResoucesLoad() {
    game.init();

    pinataDeathConfig = resources['resources/emitter.json'].data;
    pinataHitConfig = resources['resources/emitter(candyExplosion).json'].data;

    pinataDeathEmitter = new PIXI.particles.Emitter(
        // root contatiner
        particleStorage,

        // img textures array
        [
            resources['img/generalGameItems.json'].textures['candy1.png'],
            resources['img/generalGameItems.json'].textures['candy2.png'],
            resources['img/generalGameItems.json'].textures['candy3.png'],
            resources['img/generalGameItems.json'].textures['candy4.png'],
            resources['img/generalGameItems.json'].textures['candy5.png'],
        ],
        // configuration
        pinataDeathConfig
    );

    pinataHitEmitter = new PIXI.particles.Emitter(
        particleStorage,
        [
            resources['img/generalGameItems.json'].textures['candy1.png'],
            resources['img/generalGameItems.json'].textures['candy2.png'],
            resources['img/generalGameItems.json'].textures['candy3.png'],
        ],
        pinataHitConfig
    );
    // it'll play when it need
    pinataDeathEmitter.emit = false;
    pinataHitEmitter.emit = false;
}

app.stage.addChild(basicUi, pinataContainer, secretsContainer, particleStorage);
/**
 * Text styles
 */
let titleStyle = new PIXI.TextStyle({
    fill: "#630837",
    fontFamily: "\"Comic Sans MS\", cursive, sans-serif",
    fontStyle: "italic",
    fontVariant: "small-caps",
    fontWeight: 500,
    fontSize: 20
});

let menuTitleStyle = new PIXI.TextStyle({
    fill: "#fff10a",
    fontFamily: "\"Comic Sans MS\", cursive, sans-serif",
    fontStyle: "italic",
    fontVariant: "small-caps",
    fontWeight: 500,
    fontSize: 14
});

//--//Text-styles


class Game {
    constructor() {
        this.gameStart = false;
        this.gameEnd = false;
        this.playAgain = false;
        this.audio = {
            enabled: true,
            store: new Map(),
            currentlyPLay: undefined
        };
        this.config = {
            pinataDelay: false,
            settingClickDelay: false,
            defaultDelay: 150,
            elapsed: Date.now(),
            pinataCount: 5,
            playerHealth: 10,
            pinataHealth: 5
        };
        this.gameMenu = {
            general: new PIXI.Container(),
            status: 'hidden',
            options: {
                newgame: () => {
                    if (game.playAgain) this.start(true);
                    else {
                        this.gameStart = true;
                        this.showMenu('level');
                    }
                },
                resume: this.onResume,
                level: () => { this.showMenu('level'); },
                audioOn: () => {
                    let btn3 = game.UI.buttons.get('button3');
                    game.swapButton(btn3, 'audioOff');
                    game.audio.enabled = false;
                    game.stopAudio('', true);
                },
                audioOff: () => {
                    game.audio.enabled = true;
                    game.playAudio(game.audio.currentlyPLay, true);
                    let btn3 = game.UI.buttons.get('button3');
                    game.swapButton(btn3, 'audioOn');

                }
            },
            diffcultLevel: {
                easy: () => { this.config.pinataCount = 4; },
                medium: () => { this.config.pinataCount = 5; },
                hard: () => { this.config.pinataCount = 6; }
            }
        };
        // save it because later maybe it'll need to do something
        this.secret = {
            chickens: [],
        };
        this.pinata = {
            pinataHealth: 5,
            pinatas: [],
            clickMusic: undefined,
            deathMusic: undefined
        };
        this.player = {
            playerHealth: this.config.playerHealth,
            name: undefined,
            score: 0,
            attempsAmount: 0
        };
        this.text = {
            winText: "You win!",
            loseText: "You lose!",
            playerHealthEnd: "Your health is over!",
            titleText: "Lets find  the Chicken and punishing this pinatas!",
            infoText: `You have  ${this.config.playerHealth} health for search!`,
            basicUiText: new Map(),
            customText: []
        };

        this.UI = {
            buttons: new Map(),
            images: new Map(),
            mouseState: new Map([
                ['hover', "url('img/cursor2.png'),auto"],
                ['mousedown', "url('img/cursor1.png'),auto"],
                ['default', 'default'],
                ['pointer', 'pointer']
            ])
        };

        // each element that need to be animate
        // rotate - rotate animation,
        // size - size
        this.rotate = {
            candy: [],
            pinatas: [],
            buttons: []
        };
        this.size = {
            button: [],
            pinatas: []
        };
        this.arise = {
            result: []
        };
    }

    init() {
        this.createBasic();
        this.customizeMouse('custom');
        this.createMenu();
        addText(basicUi,
            { name: 'title', textContext: this.text.titleText, x: canvasW / 2, y: 100, textStyle: titleStyle },
            { name: 'infoText', textContext: this.text.infoText, x: canvasW / 2, y: 140, textStyle: titleStyle });

    }

    /**
     *
     * @param {boolean} playAgain - if game started again need to clear game items
     */
    start(playAgain) {
        this.hideMenu();
        if (playAgain === true) this.clearGameItems();
        createPinata();
        addChicken();
        if (!this.gameStart) this.gameStart = true;
        this.stopAudio(undefined, true);

        this.playAudio('bg', true);
        this.audio.currentlyPLay = 'bg';
    }
    onPause() {
        if (game.gameStart === true) {
            game.customizeMouse('default');
            game.pause = true;
            game.showMenu('pause');
        }
        else { game.showMenu(); }
    }

    onResume() {
        if (game.gameStart) {
            game.pause = false;
        }
        game.hideMenu();
    }

    playAudio(name, loop = false) {
        let audio;

        if (typeof name !== 'string') console.info("Called playAudio without name");

        if (!this.audio.enabled) return;

        try {
            audio = resources[name].sound;
            audio.loop = loop;
        } catch (error) {
            console.info('wrong audio name or it aren\'t loaded with loader');
            return;
        }

        if (!game.audio.store.has(name)) {
            game.audio.store.set(name, audio);
        }

        audio.play();

    }
    stopAudio(name, all = false) {
        if (all) {
            game.audio.store.forEach(i => { i.stop(); });
        }
        else if (game.audio.store.has(name)) {
            game.audio.store.get(name).stop();
        }

    }

    clearGameItems() {

        if (game.gameEnd) {
            game.removeFromParent([
                ...game.secret.chickens,
                game.UI.images.get('result'),
                ...game.pinata.pinatas,
                gameEndScene

            ]);
        } else if (game.playAgain) {
            game.removeFromParent([
                ...game.secret.chickens,
                ...game.pinata.pinatas
            ]);
        }

        this.gameEnd = false;
        this.pause = false;

        game.config.playerHealth = game.player.playerHealth;
        game.pinata.pinatas = [];
        game.secret = { chickens: [], candies: [] };
        game.text.basicUiText.get('infoText').text = game.text.infoText;
    }

    createBasic() {
        PIXI.loader
            .add('preview', 'resources/audio/preview.mp3')
            .load(() => {
                let audioName = "preview";
                this.playAudio(audioName, true);
                game.audio.currentlyPLay = audioName;
                PIXI.loader
                    .add('bg', 'resources/audio/bg.mp3')
                    .add('hitPinata', 'resources/audio/hitPinata.mp3')
                    .add('deathPinata', 'resources/audio/deathPinata.mp3')
                    .add('lose', 'resources/audio/lose.mp3')
                    .add('win', 'resources/audio/win.mp3')
            });

        this.startGameTrigger();
        let bg = this.createSprite({
            textureAtlas: true,
            name: "bg2.png",
            url: `img/generalGameItems.json`,
            x: canvasW / 2,
            y: canvasH / 2,
            w: applicationSize.w,
            h: applicationSize.h,
            anchor: { x: 0.5, y: 0.5 },
            intv: false
        });

        let settings = this.createSprite({
            textureAtlas: true,
            name: 'settings.png',
            url: `img/menu.json`,
            x: applicationSize.w - 50,
            y: 50,
            w: 50,
            h: 50,
            anchor: { x: 0.5, y: 0.5 },
            intv: true
        });
        settings
            .on('pointerdown', this.onSettigsPointerDown);

        game.UI.images.set('bg', bg);
        game.UI.images.set('settings', settings);
        basicUi.addChild(bg, settings);
    }
    /**
     * create menu of choose diffcult level
     */
    createMenu() {
        let data = [
            'img/menu.json', 'menu-bgEmpty.png', 'newgame.png', 'level.png', 'audioOn.png',
        ],
            length = data.length;

        for (let i = 1; i < length; i++) {
            let tmp = i === 1;

            let sprite = this.createSprite({
                textureAtlas: true,
                name: data[i],
                url: data[0],
                x: canvasW / 2,
                y: tmp ? canvasH / 1.9 : 200 + i * 53,
                w: tmp ? 280 : 188,
                h: tmp ? 319 : 40,
                anchor: { x: 0.5, y: 0.5 },
                intv: tmp ? false : true
            });

            let name = data[i].match(/^(\w[^.]+)/g)[0];
            sprite.buttonName = name;

            sprite.on('pointerdown', this.onMenuButtonPointerDown);


            if (tmp === false) {
                game.UI.buttons.set(`button${i - 1}`, sprite);
                game.gameMenu.general.addChild(sprite);
            }

            // tmp its boolean when index === index of bg
            if (tmp === true) {
                game.UI.images.set(name, sprite);
                game.gameMenu.general.addChild(sprite);

            }
            if (!game.text.basicUiText.has('menuLvlTitleText')) {
                let text = new PIXI.Text('Choose level of diffcult!', menuTitleStyle);
                let bg = game.UI.images.get('menu-bgEmpty');
                text.x = bg.x;
                text.y = bg.y - bg.height / 3;
                text.anchor.x = 0.5;
                game.text.basicUiText.set('menulvlTitleText', text);
            }
        }
    }
    /**
     * 
     * @param {event menu object } e - contains clicked button 
     */
    onMenuButtonPointerDown(e) {

        let propertyName = e.target.buttonName;

        if (game.gameMenu.status === 'setDiffcult' &&
            (propertyName === 'easy' || propertyName === "medium" || propertyName === 'hard')
        ) {
            game.gameMenu.diffcultLevel[propertyName]();

            if (game.gameStart && !game.pause && !game.playAgain) {
                game.start();
            } else {
                game.playAgain = true;
                game.showMenu();
            }

            //remove level title text 
            game.text.basicUiText.get("menulvlTitleText").visible = false;

            let diffcultLevelObject = game.text.basicUiText.get('diffcultLevel'),
                diffcultLevelText = `level: ${propertyName}`;
            // if not created a text, then create it
            if (diffcultLevelObject === undefined) {
                let textLevel = new PIXI.Text(diffcultLevelText, {
                    ...menuTitleStyle,
                    fontSize: 20
                });
                textLevel.x = 10;
                textLevel.y = 10;
                game.text.basicUiText.set('diffcultLevel', textLevel);
                basicUi.addChild(textLevel);
            } else {
                // else changing only text of level of diffcult
                diffcultLevelObject.text = diffcultLevelText;
            }

        } else if (game.gameMenu.status === 'main') {
            game.gameMenu.options[propertyName]();
        }
    }


    /**
     *
     * @param {object} props - props for create Sprite (x,y,width,heigh,anchor,url to image...)
     */
    createSprite(props) {

        let sprite = new Sprite(props.textureAtlas ?

            resources[props.url].textures[props.name] :
            PIXI.loader.resources[props.url].texture
        );

        sprite.x = props.x;
        sprite.y = props.y;
        sprite.width = props.w;
        sprite.height = props.h;
        sprite.anchor.x = props.anchor ? props.anchor.x : 0.5;
        sprite.anchor.y = props.anchor ? props.anchor.y : 0.5;
        if (props.intv) this.addInteractive(sprite, { i: true, b: true });
        return sprite;

    }

    // pretty good button that can increase and decreaseown size)
    startGameTrigger() {

        let startTrigger = this.createSprite({
            textureAtlas: true,
            name: "startGame.png",
            url: 'img/generalGameItems.json',
            x: canvasW / 2,
            y: canvasH / 2,
            w: 180,
            h: 180,
            anchor: { x: 0.5, y: 0.5 },
            intv: true
        });
        startTrigger.on("pointerdown", e => {

            game.showMenu()
            e.target.destroy();
            this.size.button.pop()
        });
        app.stage.addChild(startTrigger)
        this.UI.buttons.set('startTrigger', startTrigger);
        this.size.button.push(startTrigger)
    }
    /**
     * 
     * @param {String} menuName - selector which menu is need now
     */
    showMenu(menuName) {

        switch (menuName) {
            case "level":
                game.setButtonsToDefault(menuName);

                let titleTxt = game.text.basicUiText.get('menulvlTitleText');

                if (game.gameMenu.general.children.indexOf(titleTxt !== -1)) {
                    game.gameMenu.general.addChild(titleTxt);
                } else { titleTxt.visible = true; }

                game.gameMenu.status = 'setDiffcult';

                //exit
                break;
            case 'pause':
                if (game.UI.buttons.get('button1').buttonName !== 'resume') {
                    game.setButtonsToDefault(menuName);
                }
                game.gameMenu.status = 'main'

                //exit
                break;
            default:
                game.gameMenu.status = 'main'
                game.setButtonsToDefault();

                //exit
                break;
        }
        app.stage.addChild(game.gameMenu.general);
    }

    hideMenu() {
        if (game.gameMenu.status !== 'hidden') {
            game.removeFromParent([game.gameMenu.general]);
            game.gameMenu.status = 'hidden';
            game.customizeMouse('custom');
        }
    }

    /**
     * 
     * @param {String} option -  hint of which code need to run 
     */
    setButtonsToDefault(option) {
        let
            btn1 = game.UI.buttons.get('button1'),
            btn2 = game.UI.buttons.get('button2'),
            btn3 = game.UI.buttons.get('button3');

        if (option === 'level') {
            game.swapButton(btn1, 'easy');
            game.swapButton(btn2, 'medium');
            game.swapButton(btn3, 'hard');
        }
        else {

            if (btn1.buttonName !== 'newgame' || option === 'pause') {
                game.swapButton(btn1, option === "pause" ? 'resume' : "newgame");
            }
            if (btn2.buttonName !== 'level') {
                game.swapButton(btn2, "level");
            }
            if ((btn3.buttonName !== 'audiOn') || (btn3.buttonName !== 'audiOff')) {
                game.swapButton(btn3, game.audio.enabled ? 'audioOn' : 'audioOff');
            }
        }
    }


    /**
     * 
     * @param {PIXI Sprite} obj - one of three button if using for menu
     * @param {String} textureName - name of button image
     */
    swapButton(obj, textureName) {
        obj.texture = resources['img/menu.json'].textures[`${textureName}.png`];
        obj.buttonName = textureName;
    }

    /**
     * 
     * @param {event object} e - contains  pinata sprite
     */
    onPinataDown(e) {

        if (game.gameStart === true && !game.pause && !game.config.pinataDelay) {
            // set delay that'll be invert after default delay is end
            game.config.pinataDelay = true;
            this.isdown = true;
            if (this.isOver) {
                e.target.cursor = 'mousedown';
            }
            game.playAudio('hitPinata');
            // check target 
            logic.startCheck(e);

            // animate target sprite
            game.animationHitPinata(e.target);

            //animate candies
            game.animateCandy(e.target);

            // delay for click
            setTimeout(() => { game.config.pinataDelay = false; }, game.config.defaultDelay);
        }
    }

    /**
    * 
    * @param {event over object} e - contains pinata target 
    */
    onPinataOver(e) {
        this.isOver = true;
        if (this.isdown) {
            return;
        }
        e.target.cursor = "hover"
    }

    /**
     * 
     * @param {event up object} e - contains pinata target 
     */
    onPinataUp(e) {
        this.isdown = false;
        if (this.isOver) {
            e.target.cursor = 'hover';
        }
    }

    /**
     * 
     * @param {event pointerdown object} e - contains sprite setting button on right top if it pointerdown triggered
     */
    onSettigsPointerDown(e) {

        if (game.config.settingClickDelay || !game.gameStart) return;

        if (game.gameMenu.status !== 'hidden') {
            game.onResume();
        }
        else {
            game.onPause();
        }

        let animationTime = 300;
        let target = e.target;
        game.rotate.buttons.push({
            object: target,
            rotate: -0.08,
            name: "settings"
        });

        game.config.settingClickDelay = true;
        setTimeout(function () {
            game.rotate.buttons.find((item, index) => {
                if (item.name === "settings")
                    game.rotate.buttons.splice(index, 1);

            });
            game.config.settingClickDelay = false;

        }, animationTime);
    }

    /**
     * 
     * @param {event pointerdown object} e - if game is end, arise two button and this function handler for it'll be clicked 
     */
    onEndGameButtonDown(e) {
        switch (e.target.buttonName) {
            case 'endGameShowMenu.png':
                if (game.gameMenu.status === 'hidden') {
                    game.showMenu()
                }
                break;
            case 'playAgain.png':
                game.start(true);
                break;
        }
    }

    /**
    *
    * @param {sprite object} startPoint - x and y start position
    */
    animateCandy(pinataTargetSprite) {
        let { x, y } = pinataTargetSprite;
        pinataHitEmitter.spawnPos = { x, y };
        pinataHitEmitter.emit = true;
    }
    /**
     *
     * @param {object} pinataSprite which pinata was clicked
     */
    animationHitPinata(pinataSprite) {

        pinataSprite.scale.x /= 1.2;
        pinataSprite.scale.y /= 1.2;
        pinataSprite.rotation -= 15 / 57;

        setTimeout(function () {
            pinataSprite.scale.x *= 1.2;
            pinataSprite.scale.y *= 1.2;
            pinataSprite.rotation += 15 / 57;
        }, 200);

    }
    /**
     *
     * @param {object} sprite onDeath animate sprite as pinata
     */
    animateDeathPinata(sprite) {
        game.playAudio('deathPinata');
        pinataDeathEmitter.spawnPos = { x: sprite.x, y: sprite.y }
        pinataDeathEmitter.emit = true;
        sprite.parent.removeChild(sprite);
    }

    updateEmitters() {
        if (!pinataDeathEmitter || !pinataHitEmitter) return;
        let now = Date.now();
        // The emitter requires the elapsed
        // number of seconds since the last update
        pinataDeathEmitter.update((now - game.elapsed) * 0.001);
        pinataHitEmitter.update((now - game.elapsed) * 0.001);
        game.elapsed = now;
    }

    /**
    *
    * @param {PIXI object} obj - any sprite or graphics or something else
    */
    addInteractive(obj, props = { i: true, b: false }) {
        obj.interactive = props.i;
        obj.buttonMode = props.b;
    }
    /**
     *
     * @param {String} customMouse
     */
    customizeMouse(mouseType) {
        let hover, mousedown;
        switch (mouseType) {
            case 'custom':
                hover = game.UI.mouseState.get('hover');
                mousedown = game.UI.mouseState.get('mousedown');
                break;
            case 'default':
                hover = game.UI.mouseState.get('default');
                mousedown = game.UI.mouseState.get('default');
                break;
        }
        //Add custom cursor styles
        app.renderer.plugins.interaction.cursorStyles.hover = hover;
        app.renderer.plugins.interaction.cursorStyles.mousedown = mousedown;
    }

    /**
     *
     * @param {array} array - remove listener for each elem in
     */
    removeListener(array) { array.forEach(i => { i.removeAllListeners() }) }


    /**
     *
     * @param  {array} pixiItems - arraay things that'll remove from parent if it's exist
     */
    removeFromParent(pixiItems) {
        for (let item of pixiItems) {
            if (item.parent !== null && typeof item.parent.removeChild === 'function') {
                item.parent.removeChild(item);
            };
        }
    }

    endGameScene() {
        let flag = true,
            i = 2
        while (i--) {
            let filesPath = 'img/menu.json',
                textureName = flag ? 'playAgain.png' : 'endGameShowMenu.png';

            let sprite = this.createSprite({
                textureAtlas: true,
                name: textureName,
                url: filesPath,
                x: flag ? canvasW / 2 - 110 : canvasW / 2 + 110,
                y: 600,
                w: 188,
                h: 40,
                intv: true
            });
            flag = false;
            sprite.playAgain = true;
            sprite.buttonName = textureName;
            gameEndScene.addChild(sprite);
            gameEndScene.buttonCreated = true;
            sprite
                .on('pointerdown', this.onEndGameButtonDown);
        }
    }

    /**
     *
     * @param {boolean} result - true(win) or false(lose)
     */
    end(result) {
        // create buttons play againg and change diffcult level
        if (gameEndScene.buttonCreated !== true) game.endGameScene();

        // remove possible to click on pinata
        game.removeListener(game.pinata.pinatas);

        // on Game and with lose
        if (!result) {
            game.text.basicUiText.get('infoText').text = game.text.playerHealthEnd;
        }

        // stop bg looped audio
        game.stopAudio('bg');

        // selecting  an image and sound with dependency from  the result to show it to player
        let resultName = result ? 'win' : "lose";

        game.playAudio(resultName, true);
        game.audio.currentlyPLay = resultName;

        // a result game sprite that'll be showed player
        let playerResult = this.createSprite({
            textureAtlas: true,
            name: `${resultName}.png`,
            url: 'img/generalGameItems.json',
            x: canvasW / 2,
            y: canvasH / 2,
            w: 1,
            h: 1,
        });

        // it need to store because it'll remove from render when game start again
        game.UI.images.set('result', playerResult);
        gameEndScene.addChild(playerResult);

        setTimeout(() => {
            // adding animation of arise
            this.arise.result.push(playerResult);

            //and show buttons if player would continue play game
            app.stage.addChild(gameEndScene);
        }, 500);

        game.gameEnd = true;
        game.pause = false;
        game.gameStart = false;

        //set game status to played 
        if (!game.playAgain) game.playAgain = true;
    }
}

class Logic extends Game {
    constructor() {
        super();
    }
    /**
     * 
     * @param {event object} e - not own, it get external from handler pinata click 
     */
    startCheck(e) {

        if (game.gameEnd === true) return;
        game.config.playerHealth -= 1;
        e.target.pinataHealth -= 1;


        this.status = {
            player: logic.checkPlayer(),
            pinata: logic.checkPinata(e.target)
        }
        let isPinataDeath = this.status.pinata[0],
            isPinataHasSecret = this.status.pinata[1];

        if (isPinataDeath) {
            game.removeListener([e.target]);
            game.animateDeathPinata(e.target);

            if (isPinataHasSecret) {
                secretsContainer.addChild(e.target.chicken);
                game.end(true);
                return;
            }
        }
        if (this.status.player) {
            game.end(false);
            return;
        }
        let infoText = game.text.basicUiText.get('infoText');
        infoText.text = infoText.text.replace(
            /\d{1,2}/g,
            `${game.config.playerHealth}`
        );
    }

    checkPlayer() { return game.config.playerHealth === 0; }

    /**
     *
     * @param {object} pinata - sprite
     */
    checkPinata(pinata) {
        return [pinata.pinataHealth === 0, pinata.hasOwnProperty("chicken")]
    }
}

//
let game = new Game(),
    logic = new Logic();

function createPinata() {

    let amount = game.config.pinataCount;
    let startPoint;

    switch (amount) {
        case 4: startPoint = canvasW / 2 - 150; break;
        case 5: startPoint = canvasW / 2 - 200; break;
        case 6: startPoint = canvasW / 2 - 250; break;
    }
    for (let i = 0; i < amount; i++) {
        /**
         * create and set basic props
         */
        let pinata = game.createSprite({
            textureAtlas: true,
            name: Math.random() > 0.5 ? '1.png' : '2.png',
            url: 'img/generalGameItems.json',
            x: startPoint + i * 100,
            y: 490,
            w: 100,
            h: 100,
            anchor: { x: 0.5, y: 0.5 },
            intv: true
        });
        pinata.pinataHealth = game.config.pinataHealth;

        /**
         * add eventlistener 
         */
        pinata
            .on("pointerdown", game.onPinataDown)
            .on('pointerover', game.onPinataOver)
            .on('pointerup', game.onPinataUp);

        /**
         * save exemplar in array for further sipmle access
         */
        game.pinata.pinatas.push(pinata);
        pinataContainer.addChild(pinata);
    }
}

function addChicken() {

    /**
    * It will  add secret chicken to random an pinata, that will show yourself only when a pinata will be destroyed
    */
    let randomPinata = game.pinata.pinatas[Math.floor(Math.random() * ((game.config.pinataCount - 0) + 0))];
    let chicken = game.createSprite({
        textureAtlas: true,
        name: 'chicken2.png',
        url: 'img/generalGameItems.json',
        x: randomPinata.x,
        y: randomPinata.y,
        w: 100,
        h: 100,
        anchor: { x: 0.5, y: 0.5 }

    });
    /**
     * this add an info to pinata that it have has chicken and push  a chicken to chickens array
     */
    randomPinata.chicken = chicken;
    game.secret.chickens.push(chicken);

}

/**
 *
 * @param  {...any} text
 * @param {PIXI.Container} container - to which container of will be added text
 */
function addText(container, ...text) {
    for (let i of text) {
        let text = new PIXI.Text(i.textContext, i.textStyle ? i.textStyle : undefined)
        text.x = i.x;
        text.y = i.y;
        text.anchor.set(i.anchor ? i.anchor : 0.5, 0.5);
        game.text.basicUiText.set(i.name, text);
        container.addChild(text);
    }
}

let state = play,
    rotateCount = 0.2,
    sizeCount = -1;

function play() {
    animate();
}

function animate() {
    if (pinataDeathEmitter || pinataHitEmitter) game.updateEmitters();

    // // candy rotate
    // game.rotate.candy.forEach((item) => {
    //     item.rotation += rotateCount;
    // });

    // pinata rotate
    game.rotate.pinatas.forEach((item) => {
        item.rotation -= rotateCount * 3;
    });
    game.rotate.buttons.forEach((item) => {
        item.object.rotation += item.rotate;
    });

    // animation of the size of the button
    game.size.button.forEach((item) => {
        if (item.width < 130 && item.height < 130) {
            sizeCount = 1
        }
        item.width += sizeCount;
        item.height += sizeCount;
        if (item.width > 180 && item.height > 180) sizeCount = -1;
    });

    //  animation of the size of the pinata
    // game.size.pinatas.forEach((item) => {
    //     let bool = item.width > 1 && item.height > 1;
    //     if (bool) {
    //         item.width -= Math.abs(3 * sizeCount);
    //         item.height -= Math.abs(3 * sizeCount);
    //     }
    //     else {
    //         item.parent.removeChild(item);
    //         game.size.pinatas.pop();
    //         game.rotate.pinatas.pop();

    // animation of the appearance of the result
    game.arise.result.forEach((item) => {
        if (item.width < 97 && item.height < 97) {
            item.width += 3;
            item.height += 3;
            item.rotation += 1.1782;
        }
    });
}

app.ticker.add(del => {
    state();
    // c.update()
});